package com.elavon.jtutorial.wrapper;

public class WrapperDemo {

	public static void main(String[] args) {
		int sides = 3;
		String shape ="";
		switch (sides) {
		default:
            System.out.println("Unsupported shape!");
            
        case 3:
            shape = "triangle";
                       
        case 4:
            shape = "quadrilateral";
            
        case 5:
            shape = "pentagon";
            
        case 6:
            shape = "hexagon";
            
        
    }
		System.out.println(shape);
}


	
	
//	static int add(int a, int b){
//		return a + b;
//	}
	
	static double add(double a, double b){
		return a + b;
	}

}
