package com.elavon.jtutorial.loop;

public class LetterLoop {

	public static void main(String[] args) {
		for (char letter = 'a'; letter <= 'z'; letter++) {
			System.out.print(letter + " ");
		}

	}

}
