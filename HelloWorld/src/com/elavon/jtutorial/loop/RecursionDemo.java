package com.elavon.jtutorial.loop;

public class RecursionDemo {
	
	static long fibo(int n) {
		if(n<2) {
			return n;
		}
		
		return fibo(n-1) + fibo(n-2);		
	}
	
	static int factorial(int n) {
		if(n == 1 || n == 0) {
			return n;
		}
		return n* factorial(n-1);
				
	}
	
	public static void main(String[] args) {
//		System.out.println(RecursionDemo.fibo(0));
		
//		for (int i=0; i<=50; i++){
//			System.out.println("f("+ i +") = "+ RecursionDemo.fibo(i));	
//			
//		}
		
		System.out.println(RecursionDemo.factorial(5));
		
	}

}
