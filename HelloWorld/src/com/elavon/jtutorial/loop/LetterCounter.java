package com.elavon.jtutorial.loop;

public class LetterCounter {
	static int countLetterInPhrase(char findChar, String phrase) {
		int findCharCount = 0;
		for (int pos = 0; pos < phrase.length(); pos++) {

			if (phrase.charAt(pos) != findChar) {
				continue;
			}
			findCharCount++;

		}
		
		return findCharCount;
	}
	

	public static void main(String[] args) {
		char findChar = 'l';
		String phrase = "quick brown fox jumps over the lazy dog";

		

		System.out.println("Found " + LetterCounter.countLetterInPhrase(findChar, phrase) + " " + findChar
				+ "'s on the phrase:");
		System.out.println(phrase);

	}

}
