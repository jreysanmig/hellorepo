package com.elavon.jtutorial.loop;

public class NestedForAssignment {
	
	static void printRightTriangle(int height) {		
		for (int x = 1; x <= height; x += 1) {
			for (int y = 0; y < x; y++) {
				System.out.print("*");	
			}
			System.out.println();	
		}
	}
	
	static void printRightTriangleInverted(int height) {		
		for (int x = height; x >= 1 ; x--) {
			for (int y = 0; y < x; y++) {
				System.out.print("*");	
			}
			System.out.println();	
		}
	}
	
	static void printHalfDiamond(int height) {		
		printRightTriangle(height);
		printRightTriangleInverted(height-1);
	}

	public static void main(String[] args) {
//		NestedForAssignment.printRightTriangle(6);
//		NestedForAssignment.printRightTriangleInverted(6);
		NestedForAssignment.printHalfDiamond(10);
		
	}

}
