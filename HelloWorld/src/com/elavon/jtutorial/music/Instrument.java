package com.elavon.jtutorial.music;


public class Instrument {
	void play() {
		System.out.println("Instrument is playing");
	}

	public static void main(String[] args) {
		Instrument i = new Instrument();
//		i.play();
		
		Guitar g = new Guitar();
		g.play();
		g.play(2);
		
		int a;
		String importPlayer;
	}
}

class Guitar extends Instrument {
//	void play() {
//		System.out.println("Guitar is playing");
//	}
	
	void play(int note) {
		System.out.println("Guitar is playing note " + note );
	}
}
